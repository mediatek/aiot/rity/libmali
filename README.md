# libmali

libmali provides Mali prebuilt user space libraries for GPU in AIOT Yocto.



## Kernel space

In kernel space, it works with the Mali GPU kernel driver (linux-mtk/drivers/gpu/arm/midgard)



## Branch

- The main branch is deprecated. 

- For Yocto Kirkstone: use `kirkstone` branch
- For Yocto Honister: use `honister` branch
- For Yocto Dunfell: use `dunfell` branch