# Copyright (C) 2020  BayLibre, SAS
# Author: Fabien Parent <fparent@baylibre.com>

LIBDIR ?= "/usr/lib"
DATADIR ?= "/usr/share"
INCLUDEDIR ?= "/usr/include"
SYSCONFDIR ?= "/etc"
MALI_PATH="$(SOC)/$(MALI_VERSION)"

ifndef SOC
$(error SOC is unsdefined)
endif

ifndef MALI_VERSION
$(error MALI_VERSION is undefined)
endif

LIBS=$(MALI_PATH)/usr/lib/*.so*

all:

install:
	install -d $(INCLUDEDIR)
	install -d $(INCLUDEDIR)/EGL
	install -d $(INCLUDEDIR)/GL
	install -d $(INCLUDEDIR)/GL/internal
	install -d $(INCLUDEDIR)/GLES
	install -d $(INCLUDEDIR)/GLES2
	install -d $(INCLUDEDIR)/GLES3
	install -d $(INCLUDEDIR)/KHR
	install $(MALI_PATH)/usr/include/*.h $(INCLUDEDIR)
	install $(MALI_PATH)/usr/include/EGL/* $(INCLUDEDIR)/EGL
	install $(MALI_PATH)/usr/include/GL/*.h $(INCLUDEDIR)/GL
	install $(MALI_PATH)/usr/include/GL/internal/* $(INCLUDEDIR)/GL/internal
	install $(MALI_PATH)/usr/include/GLES/* $(INCLUDEDIR)/GLES
	install $(MALI_PATH)/usr/include/GLES2/* $(INCLUDEDIR)/GLES2
	install $(MALI_PATH)/usr/include/GLES3/* $(INCLUDEDIR)/GLES3
	install $(MALI_PATH)/usr/include/KHR/* $(INCLUDEDIR)/KHR

	install -d $(SYSCONFDIR)/OpenCL/vendors/
	install $(MALI_PATH)/etc/OpenCL/vendors/libmali.icd $(SYSCONFDIR)/OpenCL/vendors/

	install -d $(LIBDIR)
	cp -a $(LIBS) $(LIBDIR)

	install -d $(LIBDIR)/pkgconfig
	install $(MALI_PATH)/usr/lib/pkgconfig/*.pc $(LIBDIR)/pkgconfig

	install -d $(DATADIR)/vulkan
	install -d $(DATADIR)/vulkan/icd.d
	install $(MALI_PATH)/usr/share/vulkan/icd.d/mali.json \
		$(DATADIR)/vulkan/icd.d/mali.json
